import { MorbiPage } from './app.po';

describe('morbi App', function() {
  let page: MorbiPage;

  beforeEach(() => {
    page = new MorbiPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
