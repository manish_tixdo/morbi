import { BrowserModule } from '@angular/platform-browser';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { enableProdMode, NgModule } from '@angular/core';
import { AppComponent, environment } from './app/';
import { AngularFireModule } from 'angularfire2';
import { AppModule } from './app/app.module';


platformBrowserDynamic().bootstrapModule(AppModule);
// Must export the config


// @NgModule({
//   imports: [
//     BrowserModule,
//     AngularFireModule.initializeApp(firebaseConfig)
//   ],
//   declarations: [ AppComponent ],
//   bootstrap: [ AppComponent ]
// })
export class MyAppModule {}
