import { Component, OnInit } from '@angular/core';
import { AngularFire, AuthProviders, AuthMethods } from 'angularfire2';
import {NgIf} from '@angular/common';
@Component({
  moduleId: module.id,
  selector: 'app-navbar',
  templateUrl: 'navbar.component.html',
  styleUrls: ['navbar.component.css']
})
export class NavbarComponent implements OnInit {

  isUserLoggedIn:boolean=false;
  //photoUrl: any
  currentUser: any;
  constructor(private af: AngularFire) {
    // this.af.auth.subscribe(auth => this.currentUser = auth
    //
    // );


  }

  login() {

    this.af.auth.login({
      provider: AuthProviders.Google,
      method: AuthMethods.Popup,
    });
    this.af.auth.subscribe(data => {
      if (data)
      {
        this.isUserLoggedIn = true;
      }
      }
    );



    //console.log(this.currentUser)
    //this.photoUrl = this.currentUser.google.photoURL
    //console.log(this.photoUrl)

  }

  logOut() {
    this.af.auth.logout();
    // this.isUserLoggedIn = false;
  this.af.auth.subscribe(data => {
    if (data)
    {
      this.isUserLoggedIn = false;
    }

  });

  }
  ngOnInit() {
  }

}
