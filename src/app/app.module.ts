import { NgModule }       from '@angular/core';
import { BrowserModule  } from '@angular/platform-browser';
import { AppComponent }   from './app.component';
import { AngularFireModule } from 'angularfire2';
import {NavbarComponent} from "./navbar/navbar.component";
import {FIREBASE_PROVIDERS,
  defaultFirebase,
  AngularFire,
  AuthMethods,
  AuthProviders,
  firebaseAuthConfig} from 'angularfire2';


export const firebaseConfig = {
  apiKey: "AIzaSyCLXegKhbDnvj7eZ36pD92MoRb380307lQ",
  authDomain: "morbi-c5db0.firebaseapp.com",
  databaseURL: "https://morbi-c5db0.firebaseio.com",
  storageBucket: "morbi-c5db0.appspot.com",
};
const myFirebaseAuthConfig = {
  provider: AuthProviders.Google,
  method: AuthMethods.Redirect,
}
@NgModule({
    declarations: [AppComponent, NavbarComponent],
    imports:      [BrowserModule,
    AngularFireModule.initializeApp(firebaseConfig, myFirebaseAuthConfig)
    ],
    bootstrap:    [AppComponent],

})
export class AppModule {

}
